#include <iostream>
#include <fstream>
#include <sstream>

#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/console/parse.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>

void read_matrix(Eigen::Matrix4f *trans) {
    char line[256];
    int i;
    for (i = 0; i < 4; i++) {
        double x, y, z, w;
        std::cin.getline(line, 256);
        std::istringstream li(line);
        li >> x
           >> y
           >> z
           >> w;
        (*trans) (i, 0) = x;
        (*trans) (i, 1) = y;
        (*trans) (i, 2) = z;
        (*trans) (i, 3) = w;
    }
}

int main(int argc, char **argv) {
    Eigen::Matrix4f trans;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr source_cloud(new pcl::PointCloud<pcl::PointXYZRGB>());
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr dest_cloud(new pcl::PointCloud<pcl::PointXYZRGB>());
    if (argc < 3) {
        std::cerr << "usage: "
                  << argv[0]
                  << " <input>"
                  << " <output>"
                  << " [--visualise]"
                  << std::endl;
        return 1;
    }
    read_matrix(&trans);
    if (pcl::io::loadPCDFile(argv[1], *source_cloud) != 0) {
        std::cerr << "Could not load input point cloud."
                  << std::endl;
        return 2;
    }

    pcl::transformPointCloud(*source_cloud, *dest_cloud, trans);
    pcl::io::savePCDFileASCII(argv[2], *dest_cloud);

    if (!(argc >= 4 && strcmp(argv[3], "--visualise") == 0)) {
        return 0;
    }

    pcl::visualization::PCLVisualizer viewer ("torsion");
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> source_colour (source_cloud, 255, 255, 255);
    viewer.addPointCloud(source_cloud, source_colour, "source");
    viewer.addPointCloud(dest_cloud, "dest");
    viewer.addCoordinateSystem (1.0, "cloud", 0);
    viewer.setBackgroundColor(0.05, 0.05, 0.05, 0);
    while (!viewer.wasStopped ()) {
        viewer.spinOnce ();
    }
    return 0;
}
