from setuptools import setup, find_packages
from os import path
from io import open

here = path.abspath(path.dirname(__file__))

setup(
    name='torsion',
    version='0.0.1',
    description='Rotates things.',
    packages=find_packages(exclude=['doc', 'test']),
    install_requires=[
        'numpy',
        ],
    extras_require={
        'test' : [
            'pytest',
          ],
        },
    entry_points={
        'console_scripts': [
            'torsion=torsion:main',
        ],
    },
)
