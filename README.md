torsion
=======

`torsion` generates transformation matrices for 3D data
augmentation. It is developed with Python 3.7; it will probably work
fine with any 3.x Python version.

Usage
=====

```
usage: torsion [-h] [--no-rotate] [--no-translate]
               [--max-rotation MAX_ROTATION]
               [--max-translation-deltas MAX_TRANSLATION_DELTAS]
               {generate} ...

positional arguments:
  {generate}
    generate            generate a transformation matrix

optional arguments:
  -h, --help            show this help message and exit
  --no-rotate           Leave rotation component as zero.
  --no-translate        Leave translation component as zero.
  --max-rotation MAX_ROTATION
                        Maximum rotation angle in radians (default 2π).
  --max-translation-deltas MAX_TRANSLATION_DELTAS
                        Maximum translation delta, by axis (default 5,5,5).
```

Output
======

Output of the `generate` command is a 4x4 affine transformation
matrix. By default, the rotation component is uniformly random in the
SO(3) group (a random angle around a random axis); it can optionally
be restricted via CLI options. Translations are generated as a uniform
delta with a maximum magnitude, also configurable via CLI options.

Example output:

```
$ torsion --max-rotation 0.6 --max-translation-deltas 2,10,1 generate
   0.99588053 -0.09026625 -0.00860071 -0.64865276
   0.08893928  0.9908828  -0.10119826 -2.81864033
   0.01765708  0.10001644  0.9948291  -0.4309808
   0.          0.          0.          1.
```

PCD data
========

A separate tool, `torsion-pcd`, applies the generated rotation
matrices to point-cloud data files. This component is written in C++
to link with PCL 1.8, and should be built as follows:

```
mkdir -p torsion-pcd/build
cd torsion-pcd/build
cmake ..
make
```

To generate and apply a transformation, save the result, and display
the transformed cloud, run as follows:

```
torsion generate | ./torsion-pcd input-cloud.pcd output-cloud.pcd --visualise
```

To skip the visualisation and just save the output cloud, run without
`--visualise`.