import numpy as np
from random import random
from math import pi, sin, cos

class Rotor:
    def __init__(self, theta, axis_vector):
        self.theta = theta
        self.axis_vector = axis_vector

    def matrix(self):
        return angle_axis_matrix(self.theta, self.axis_vector)

def gen_rotor(max_rotation=None):
    """Generate a Rotor object for a rotation with maximum angle in
    radians."""
    theta = gen_rotation_angle(max_rotation)
    axis_vector = gen_axis_vector()
    return Rotor(theta, axis_vector)

def skew_symmetric(u):
    """Construct skew-symmetric matrix from unit vector u."""
    u1, u2, u3 = u
    r1 = (0, -u3, u2)
    r2 = (u3, 0, -u1)
    r3 = (-u2, u1, 0)
    return np.array((r1, r2, r3))

def angle_axis_matrix(theta, u):
    """Convert rotation angle theta and rotation axis u into a 3x3 rotation
    matrix."""
    ux = skew_symmetric(u)
    i3 = np.identity(3)
    tensor = np.outer(u, u)
    r = cos(theta) * i3 + sin(theta) * ux + (1 - cos(theta)) * tensor
    return r

def gen_rotation_angle(max_rotation=None):
    u = random()
    if max_rotation is None:
        return u * pi * 2
    else:
        return u * max_rotation

def gen_polar():
    return np.arccos(2*random() -  1)

def gen_azimuth():
    return random() * pi * 2

def gen_axis_vector():
    theta = gen_polar()
    phi = gen_azimuth()
    u1 = sin(theta) * cos(phi)
    u2 = sin(theta) * sin(phi)
    u3 = cos(theta)
    return (u1, u2, u3)
