import sys
import argparse
from math import pi

from torsion.affine import Affine
from torsion.rotation import gen_rotor
from torsion.translation import gen_translator

def translation_deltas(s):
    ds = list(map(int, s.split(',')))
    if len(ds) != 3:
        raise argparse.ArgumentTypeError("translation deltas must be of form 'dx,dy,dz'")
    return (ds[0], ds[1], ds[2])

def main():
    parser = argparse.ArgumentParser(prog='torsion')
    parser.add_argument(
        '--no-rotate',
        dest='no_rotate',
        action='store_true',
        help='Leave rotation component as zero.'
        )
    parser.add_argument(
        '--no-translate',
        dest='no_translate',
        action='store_true',
        help='Leave translation component as zero.'
        )
    parser.add_argument(
        '--max-rotation',
        dest='max_rotation',
        type=float,
        default=2*pi,
        help='Maximum rotation angle in radians (default 2π).',
        )
    parser.add_argument(
        '--max-translation-deltas',
        dest='max_translation_deltas',
        type=translation_deltas,
        default='5,5,5',
        help='Maximum translation delta, by axis (default 5,5,5).'
        )

    subparsers = parser.add_subparsers(dest='command')
    generate_p = subparsers.add_parser('generate', help='generate a transformation matrix')
    args = parser.parse_args()
    if args.command == 'generate':
        generate(args)
    else:
        parser.error("No command specified.")

def generate(args):
    rotor = None
    translator = None
    if not args.no_rotate:
        rotor = gen_rotor(args.max_rotation)
    if not args.no_translate:
        translator = gen_translator(args.max_translation_deltas)
    affine = Affine(rotor, translator)
    out = str(affine.matrix()).replace('[', ' ').replace(']', ' ')
    print(out)

if __name__ == '__main__':
    main()
