import numpy as np
from random import random
from math import pi, sin, cos

def gen_delta(max_delta):
    u = random()
    return u * max_delta - max_delta / 2

def gen_translator(max_deltas=None):
    if max_deltas is None:
        max_deltas = (20, 20, 20)
    mdx, mdy, mdz = max_deltas
    dx = gen_delta(mdx)
    dy = gen_delta(mdy)
    dz = gen_delta(mdz)
    return Translator(dx, dy, dz)

class Translator:
    def __init__(self, dx, dy, dz):
        self.deltas = (dx, dy, dz)

    def matrix(self):
        return np.transpose(np.asmatrix(self.deltas))
