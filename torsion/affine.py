import numpy as np
from random import random
from math import pi, sin, cos

from torsion.translation import gen_translator
from torsion.rotation import gen_rotor

def gen_affine(max_rotation_angle=None, max_translation_deltas=None):
    """Generate affine transformation matrix with optional constraints on
    rotation angle and translation."""
    rotor = gen_rotor(max_rotation_angle)
    translator = gen_translator(max_translation_deltas)
    return Affine(rotor, translator)

class Affine:
    def __init__(self, rotor=None, translator=None):
        if rotor is None:
            self.rotation = np.zeros((3, 3))
        else:
            self.rotation = rotor.matrix()
        if translator is None:
            self.translation = np.zeros((3, 1))
        else:
            self.translation = translator.matrix()

    def matrix(self):
        """Homogeneous transformation matrix for this affine."""
        m = np.concatenate((self.rotation, self.translation), axis=1)
        u = np.asmatrix(np.array((0, 0, 0, 1)))
        affine = np.concatenate((m, u), axis=0)
        return affine
