import numpy as np
from torsion.translation import *
import pytest

def test_gen_translator():
    t = gen_translator()
    assert t.matrix().shape == (3, 1)
