import numpy as np
import numpy.linalg as la
from math import pi
from random import random
from torsion.rotation import *
import pytest

def test_gen_rotation_angle():
    rotor = gen_rotor()
    assert rotor.theta >= 0.0 and rotor.theta < (2 * pi)

def test_gen_rotation_angle_max():
    m = random() * 2 * pi
    rotor = gen_rotor(max_rotation=m)
    assert rotor.theta >= 0.0 and rotor.theta < m

def test_gen_axis_vector():
    rotor = gen_rotor()
    assert la.norm(rotor.axis_vector) == pytest.approx(1.0)

# det R = 1
def test_gen_rotation_determinant():
    rotor = gen_rotor()
    r = rotor.matrix()
    assert la.det(r) == pytest.approx(1.0)

# R^T = R^-1
def test_gen_rotation_orthogonal():
    rotor = gen_rotor()
    r = rotor.matrix()
    assert np.allclose(np.matrix.transpose(r), la.inv(r))
