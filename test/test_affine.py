import numpy as np
from torsion.affine import *
import pytest

def test_gen_affine():
    a = gen_affine()
    assert a.matrix().shape == (4, 4)
